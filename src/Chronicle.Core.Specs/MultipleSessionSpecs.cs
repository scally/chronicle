﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NHibernate.Criterion;
using FluentNHibernate;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Cfg;

namespace Chronicle.Core.Specs
{
	public class when_supporting_multiple_session_builders
	{
		public class Cat
		{
			public virtual int Id { get; set; }
			public virtual string Name { get; set; }
			public virtual int Age { get; set; }
		}

		public class TestSessionConfiguration
		{
			public static NHibernate.Cfg.Configuration GetTestConfiguration()
			{
				return Fluently.Configure()
						.Mappings(x => x.FluentMappings.Add<CatMap>())
						.Database(SQLiteConfiguration.Standard.UsingFile("foo.db").ShowSql())
						.BuildConfiguration();
			}
		}

		public class TestPersistenceModel : PersistenceModel
		{
			public TestPersistenceModel()
			{
				Add ( typeof(CatMap) );
			}
		}

		public class CatMap : ClassMap<Cat>
		{
			public CatMap()
			{
				Id(cat => cat.Id).GeneratedBy.Identity();
				Map(cat => cat.Age);
				Map(cat => cat.Name);
			}
		}

		public class TestSessionBuilder : SingletonSessionBuilder
		{
			public TestSessionBuilder()
				: base(TestSessionConfiguration.GetTestConfiguration())
			{

			}
		}

		public class NotUsedSessionBuilder : SingletonSessionBuilder
		{
			public NotUsedSessionBuilder()
				: base ( GetTestConfiguration ())
			{ }

			public static NHibernate.Cfg.Configuration GetTestConfiguration()
			{
				return Fluently.Configure()
						.Mappings(x => x.FluentMappings.Add<CatMap>())
						.Database(SQLiteConfiguration.Standard.UsingFile("foo2.db").ShowSql())
						.BuildConfiguration();
			}

			public static PersistenceModel GetPersistenceModel ()
			{
				var pm = new PersistenceModel ();
				pm.Add ( typeof(CatMap));
				return pm;
			}
		}

		[Test]
		public void repository_should_return_its_results()
		{
			createSchema();
			createRepo().Save<Cat>(new Cat { Name = "Garfield" });

			var cat = createRepo().Load<Cat>(1);

			var criteria = DetachedCriteria.For<Cat>();
			criteria.Add(Expression.Eq("Name", "Garfield"));

			var repo = createRepo();
			var cats = repo.Find<Cat>(criteria);

			Assert.That(cats.Count, Is.EqualTo(1));
			Assert.That(cats[0].Name, Is.EqualTo("Garfield"));
		}

		protected void createSchema()
		{
			new SchemaExport(new TestSessionBuilder().GetConfiguration())
				.Create(false, true);
		}

		protected IRepository createRepo()
		{
			//TODO: move IoC registration to Core, use that instead
			return new NHibernateRepository(
				new NHibernateUnitOfWork(
					new SingletonSessionBuilder [] { 
						new TestSessionBuilder(), 
						new NotUsedSessionBuilder () }));
		}
	}
}
