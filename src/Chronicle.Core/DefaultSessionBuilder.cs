﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
	public class DefaultSessionBuilder : SingletonSessionBuilder
	{
		public DefaultSessionBuilder(ISessionConfiguration config)
			: base(config.GetConfiguration()) { }
	}
}
