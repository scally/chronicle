﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Chronicle
{
	public interface INHibernateUnitOfWork : IUnitOfWork, IDisposable
    {
        ISession GetSession<ENTITY>();
        void CleanUp();
    }
}
