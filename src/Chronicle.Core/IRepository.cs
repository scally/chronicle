﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

using NHibernate.Criterion;
using NHibernate.Linq;

namespace Chronicle
{
    public interface IRepository
    {
		IUnitOfWork UnitOfWork { get; }
		void SaveOrUpdate<ENTITY>(ENTITY obj);
		void SaveOrMerge<ENTITY>(ENTITY obj);
		void Save<ENTITY>(ENTITY obj);
		void Update<ENTITY>(ENTITY obj);
		ENTITY Load<ENTITY>(int id);
		ENTITY Load<ENTITY>(Guid guid);
		ENTITY Load<ENTITY>(Expression<Func<ENTITY, bool>> predicate);
		void Delete<ENTITY>(ENTITY obj);
		void Delete<ENTITY>(int id);
		void Delete<ENTITY>(Guid id);
		void Delete<ENTITY>(Expression<Func<ENTITY, bool>> predicate);
		IList<ENTITY> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate);
		IList<ENTITY> Find<ENTITY>(DetachedCriteria criteria);
		SearchResults<ENTITY> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate, QuerySpec<ENTITY> querySpec);
		INHibernateQueryable<ENTITY> GetQueryable<ENTITY>();
		IList<ENTITY> FindAll<ENTITY>();
		long Count<ENTITY>();
		long Count<ENTITY>(Expression<Func<ENTITY, bool>> predicate);
		/// <summary>
		/// HACK: This is temporarily added as a stopgap for non-web clients
		/// </summary>
		void RefreshCache<ENTITY>();
		IList<object> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate, IList<Expression<Func<ENTITY, object>>> selectors);
    }
}
