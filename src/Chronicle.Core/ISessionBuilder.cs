﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Chronicle
{
    public interface ISessionBuilder
    {
        ISession GetSession();
    }
}
