﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Chronicle
{
	public interface ISessionLifeCycle
	{
		bool HasSession(string key);
		ISession GetSession(string key);

		ISession AddSession(string key, ISession session);
		void CloseSessions();
	}
}
