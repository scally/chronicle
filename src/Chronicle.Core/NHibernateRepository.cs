﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Chronicle
{
    public class NHibernateRepository : IRepository
    {
        protected INHibernateUnitOfWork unitOfWork;

        public NHibernateRepository(INHibernateUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork
        {
            get { return unitOfWork; }
        }

        public virtual void Save<ENTITY>(ENTITY obj)
        {
            ensureCleanup(() =>
            {
                getSession<ENTITY>().Save(obj);
                getSession<ENTITY>().Flush();
            });
        }

		public virtual void SaveOrMerge<ENTITY>(ENTITY obj)
		{
			ensureCleanup(() =>
			{
				getSession<ENTITY>().SaveOrUpdateCopy(obj);
				getSession<ENTITY>().Flush();
			});
		}

		public virtual void SaveOrUpdate<ENTITY>(ENTITY obj)
		{
			ensureCleanup(() =>
			{
				getSession<ENTITY>().SaveOrUpdate(obj);
				getSession<ENTITY>().Flush();
			});
		}

		public virtual void Update<ENTITY>(ENTITY obj)
		{
			ensureCleanup(() =>
			{
				getSession<ENTITY>().Update(obj);
				getSession<ENTITY>().Flush();
			});
		}

        public virtual ENTITY Load<ENTITY>(int id)
        {
            var entity = default(ENTITY);
            ensureCleanup(() =>
            {
                entity = getSession<ENTITY>().Get<ENTITY>(id);
            });
            return entity;
        }

        public virtual ENTITY Load<ENTITY>(Guid guid)
        {
            var entity = default(ENTITY);
            ensureCleanup(() =>
            {
                entity = getSession<ENTITY>().Get<ENTITY>(guid);
            });
            return entity;
        }

        public virtual ENTITY Load<ENTITY>(Expression<Func<ENTITY, bool>> predicate)
        {
            var entity = default(ENTITY);
            ensureCleanup(() =>
            {
				entity = getSession<ENTITY>().Linq<ENTITY>().Where<ENTITY>(predicate).SingleOrDefault();
            });
            return entity;
        }

        public virtual IList<ENTITY> FindAll<ENTITY>()
        {
            return Find<ENTITY>(e => true);
        }

        public virtual IList<ENTITY> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate)
        {
            var entities = new List<ENTITY>();
            ensureCleanup(() =>
            {
                entities.AddRange(getSession<ENTITY>().Linq<ENTITY>().Where<ENTITY>(predicate).ToList());
            });
            return entities;
        }

		public virtual INHibernateQueryable<ENTITY> GetQueryable<ENTITY>()
		{
			return getSession<ENTITY>().Linq<ENTITY>();
		}


		public IList<object> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate, IList<Expression<Func<ENTITY, object>>> selectors)
		{
			var selector = this.BuildLambda<ENTITY>(selectors);
			return getSession<ENTITY>().Linq<ENTITY>().Where<ENTITY>(predicate).Select(selector).ToList();
		}

		private Expression<Func<ENTITY, object>> BuildLambda<ENTITY>(IList<Expression<Func<ENTITY, object>>> expressions)
		{
			Expression<Func<ENTITY, object>> combinedExpression = expressions[0];
			expressions.RemoveAt(0);

			foreach (Expression<Func<ENTITY, object>> expression in expressions)
				combinedExpression = System.Linq.Expressions.Expression.Lambda<Func<ENTITY, object>>(System.Linq.Expressions.Expression.AndAlso(expression.Body, combinedExpression.Body), expression.Parameters);

			return combinedExpression;
		}

		/// <summary>
		/// HACK: This is temporarily added as a stopgap for non-web clients. If you are using this in a web app U R DOIN IT RONG
		/// </summary>
		public virtual void RefreshCache<ENTITY>()
		{
			getSession<ENTITY>().Clear();
		}

        public virtual IList<ENTITY> Find<ENTITY>(DetachedCriteria criteria)
        {
            var entities = new List<ENTITY>();
            ensureCleanup(() =>
            {
                var executable = criteria.GetExecutableCriteria(getSession<ENTITY>());
                entities.AddRange(executable.List<ENTITY>());
            });
            return entities;
        }
        
        public virtual SearchResults<ENTITY> Find<ENTITY>(Expression<Func<ENTITY, bool>> predicate, QuerySpec<ENTITY> querySpec)
        {
            var searchResults = new SearchResults<ENTITY>();
            
            ensureCleanup(() =>
            {
				var pagedSorter = new QuerySpecApplier<ENTITY>();
                var query = getSession<ENTITY>().Linq<ENTITY>().Where<ENTITY>(predicate);

                searchResults.Total = query.Count();
                
                if ( querySpec != null )
					query = pagedSorter.Apply(query, querySpec);
                                
                searchResults.Entities = query.ToList();
            });
            
            return searchResults;
        }

        public virtual long Count<ENTITY>()
        {
            return Count<ENTITY> ( x => true );
        }

		public virtual long Count<ENTITY>(Expression<Func<ENTITY, bool>> predicate)
		{
			var count = (long)0;
			ensureCleanup( () => 
			{
				count = getSession<ENTITY>().Linq<ENTITY>().Count(predicate);
			});
			return count;
		}

        public virtual void Delete<ENTITY>(ENTITY obj)
        {
            ensureCleanup(() =>
            {
                getSession<ENTITY>().Delete(obj);
                getSession<ENTITY>().Flush();
            });
        }

        public virtual void Delete<ENTITY>(int id)
        {
            ensureCleanup(() =>
            {
                getSession<ENTITY>().Delete(getSession<ENTITY>().Load<ENTITY>(id));
                getSession<ENTITY>().Flush();
            });
        }

        public virtual void Delete<ENTITY>(Guid id)
        {
            ensureCleanup(() =>
            {
                getSession<ENTITY>().Delete(getSession<ENTITY>().Load<ENTITY>(id));
                getSession<ENTITY>().Flush();
            });
        }

        public virtual void Delete<ENTITY>(Expression<Func<ENTITY, bool>> predicate)
        {
            ensureCleanup(() =>
            {
                getSession<ENTITY>().Linq<ENTITY>().Where<ENTITY>(predicate).ToList().ForEach(e => Delete(e));
            });
        }
	
        protected virtual void ensureCleanup(Action block )
        {       
			try
			{
				block.Invoke();
			}
			catch (Exception)
			{
				this.unitOfWork.CleanUp();
				throw;
			}
        }
        
        protected ISession getSession<ENTITY>()
        {
            return this.unitOfWork.GetSession<ENTITY>();
        }
    }
}
