﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using NHibernate;
using System.Threading;

namespace Chronicle
{
    public class NHibernateUnitOfWork : INHibernateUnitOfWork, IDisposable
    {
        private bool _isDisposed;
        private bool _isInitialized;
        private SingletonSessionBuilder[] _sessionBuilders;    
		
		// this will break interfaces, so I am relucant to make it part of the ctor    
		private ISessionLifeCycle _sessionLifeCycle = new PerThreadSessionLifeCycle ();

		protected TransactionScope Transaction;

        public NHibernateUnitOfWork(SingletonSessionBuilder[] sessionBuilders)
        {
            _sessionBuilders = sessionBuilders;
            Initialize();
        }

		public ISessionLifeCycle SessionLifeCycle 
		{ 
			get { return _sessionLifeCycle; }
			set { _sessionLifeCycle = value; } 
		}

        public virtual void BeginTransaction()
        {
            Transaction = new TransactionScope();
        }

        private void EndTransaction()
        {
            if (Transaction != null)
			{
				Transaction.Dispose();
				Transaction = null;
			}
        }

        public ISession GetSession<ENTITY>()
        {
            var sessionBuilder = findSessionBuilder<ENTITY>();
			var properties = sessionBuilder.GetConfiguration ().Properties;

			var sessionBuilderKey = 
				String.Format ( "{0}{1}",
					properties["connection.connection_string"],
					properties.ContainsKey("config-source") 
						? properties["config-source"] 
						: sessionBuilder.GetType().FullName );

			if (_sessionLifeCycle.HasSession(sessionBuilderKey))
                return _sessionLifeCycle.GetSession(sessionBuilderKey);

            return _sessionLifeCycle.AddSession(sessionBuilderKey,
				sessionBuilder.GetSession ());
        }

        private SingletonSessionBuilder findSessionBuilder<ENTITY>()
        {
            if (_sessionBuilders.Length == 1)
                return _sessionBuilders.First();

            return _sessionBuilders.First(b => b.ContainsMapFor<ENTITY>());
        }

        public virtual void Commit()
        {
            should_not_currently_be_disposed();
            should_be_initialized_first();

            Transaction.Complete();
            EndTransaction();
        }

        public virtual void Rollback()
        {
            should_not_currently_be_disposed();
            should_be_initialized_first();

            EndTransaction();
        }

        public void Dispose()
        {
            if (_isDisposed || !_isInitialized) return;
            EndTransaction();
			
			_sessionLifeCycle.CloseSessions ();
            _isDisposed = true;
        }

        private void should_not_currently_be_disposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(GetType().Name);
        }

        private void should_be_initialized_first()
        {
            if (!_isInitialized) throw new InvalidOperationException("Must initialize (call Initialize()) on NHibernateUnitOfWork before commiting or rolling back");
        }

        private void Initialize()
        {
            should_not_currently_be_disposed();
            _isInitialized = true;
        }

		public virtual void CleanUp()
        {
            EndTransaction();
        }
    }
}
