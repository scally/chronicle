﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
    public enum OrderDirection
    {
        Ascending,
        Descending
    };
}
