﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
    public class QueryPage
    {
        public int Size { get; set; }
        public int Number { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is QueryPage)) return false;
            var other = (QueryPage)obj;
            return this.Size == other.Size && this.Number == other.Number;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
