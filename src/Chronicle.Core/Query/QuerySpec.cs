﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Chronicle.Query;

namespace Chronicle
{
    public class QuerySpec<ENTITY>
    {
		public List<SortExpression<ENTITY>> SortExpressions { get; set; }
        public QueryPage Page { get; set; }
        public List<LambdaExpression> Expand { get; set; } // aka eager load?
		public bool IsCacheable { get; set; }
		public string CacheRegion { get; set; }
		public bool DistinctRoot { get; set; }

		public QuerySpec ()
        {
			SortExpressions = new List<SortExpression<ENTITY>> ();
			Expand = new List<LambdaExpression> ();
        }

		public QuerySpec<ENTITY> SetDistinctRootEntity()
		{
			DistinctRoot = true;
			return this;
		}

		public QuerySpec<ENTITY> Cacheable(bool val)
		{
			IsCacheable = val;
			return this;
		}

		public QuerySpec<ENTITY> CacheToRegion(string region)
		{
			IsCacheable = true;
			CacheRegion = region;
			return this;
		}

        
        public QuerySpec<ENTITY> SortBy ( Expression<Func<ENTITY, IComparable>> orderby, OrderDirection direction )
        {
			SortExpressions.Add ( new SortExpression<ENTITY> { OrderBy = orderby, OrderByDirection = direction });
			return this;
        }
        
        public QuerySpec<ENTITY> ExpandOn<K> ( Expression<Func<ENTITY,K>> expand )
        {
			Expand.Add ( expand );
			return this;
        }

		private QueryPage getQueryPage ()
		{
			if ( Page == null ) 
				Page = new QueryPage ();
			return Page;
		}

		public QuerySpec<ENTITY> PageLengthOf(int length)
		{
			if ( length < 0 ) return this;
			
			var page = getQueryPage ();
			page.Size = length;
				
			return this;
		}

		public QuerySpec<ENTITY> StartingFromPage(int pageNumber)
		{
			if ( pageNumber < 0 ) return this;
			
			var page = getQueryPage();
			page.Number = pageNumber;
			
			// thought about doing this..
			//if ( page.Size == 0 ) 
			//    page.Size = 25;
						
			return this;
		}

        public override bool Equals(object obj)
        {
            if (!(obj is QuerySpec<ENTITY>)) return false;
            var other = (QuerySpec<ENTITY>)obj;

            return this.SortExpressions.Equals(other.SortExpressions) &&
                   this.Page.Equals(other.Page);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
