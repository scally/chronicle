﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Linq;
using Chronicle.Query;
using System.Linq.Expressions;
using NHibernate.Transform;

namespace Chronicle
{
	// name fail
    public class QuerySpecApplier<ENTITY>
    {
        public IQueryable<ENTITY> Apply(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
        {
            query = expandOn(query,spec);
            query = orderQuery(query,spec);
            query = pageQuery(query,spec);
			query = setDistinctRoot(query, spec);
			query = setCache(query, spec);

			return query;
        }

		private IQueryable<ENTITY> setDistinctRoot(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
		{
			if (!spec.DistinctRoot) return query;

			var nhQuery = query as INHibernateQueryable<ENTITY>;
			if (nhQuery == null) return query;

			nhQuery.QueryOptions.RegisterCustomAction(x => x.SetResultTransformer(new DistinctRootEntityResultTransformer()));
			return nhQuery as IQueryable<ENTITY>;
		}

		private IQueryable<ENTITY> setCache(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
		{
			if (!spec.IsCacheable) return query;

			var nhQuery = query as INHibernateQueryable<ENTITY>;
			if (nhQuery == null) return query;

			nhQuery.QueryOptions.SetCachable(true);
			if (!String.IsNullOrEmpty(spec.CacheRegion))
				nhQuery.QueryOptions.SetCacheRegion(spec.CacheRegion);

			return nhQuery as IQueryable<ENTITY>;
		}

		private IQueryable<ENTITY> expandOn(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
		{
			if ( spec.Expand == null ) return query;
		
			var nhQuery = query as INHibernateQueryable<ENTITY>;
			if ( nhQuery == null ) return query;
		
			foreach ( LambdaExpression expression in spec.Expand )
				nhQuery.Expand ( expression );
			
			return nhQuery as IQueryable<ENTITY>;
		}

        private IQueryable<ENTITY> orderQuery(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
        {
			if ( spec.SortExpressions == null ) return query;
        
			foreach ( SortExpression<ENTITY> expression in spec.SortExpressions )
			{
				if ( expression.OrderByDirection == OrderDirection.Ascending)
					query = query.OrderBy(expression.OrderBy);
				else
					query = query.OrderByDescending(expression.OrderBy);
			}
			
			return query;
        }

        private IQueryable<ENTITY> pageQuery(IQueryable<ENTITY> query, QuerySpec<ENTITY> spec)
        {
			if ( spec.Page == null ) return query;
        
            return query
                .Skip(spec.Page.Size * (spec.Page.Number - 1))
                .Take(spec.Page.Size);
        }
    }
}
