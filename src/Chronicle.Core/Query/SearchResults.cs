﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
    public class SearchResults<ENTITY>
    {
        IList<ENTITY> entities = new List<ENTITY>();
        public int Total { get; set; }

        public IList<ENTITY> Entities
        {
            get { return entities; }
            set { entities = value; }
        }
    }
}
