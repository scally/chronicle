﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Chronicle.Query
{
	public class SortExpression<ENTITY>
	{
		public SortExpression ()
		{
			OrderByDirection = OrderDirection.Ascending;
		}
		
		public Expression<Func<ENTITY, IComparable>> OrderBy { get; set; }
		public OrderDirection OrderByDirection { get; set; }
	}
}
