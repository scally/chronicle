﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SessionBuilderAttribute : Attribute
    {
        public Type SessionBuilderType { get; private set; }

        public SessionBuilderAttribute(Type sessionBuilderType)
        {
            if (!(sessionBuilderType.IsSubclassOf(typeof(SingletonSessionBuilder))))
                throw new ArgumentException("SessionBuilderType must derive from SingletonSessionBuilder");

            SessionBuilderType = sessionBuilderType;
        }
    }
}
