﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate;
using FluentNHibernate.Mapping;

namespace Chronicle
{
	public class SingletonSessionBuilder : ISessionBuilder
	{
		private static object syncObject = new object ();
		private static Dictionary<string, ISessionFactory> _factories = new Dictionary<string, ISessionFactory>();
		private static HashSet<ISession> allSessions = new HashSet<ISession>();

		protected IDictionary<string, string> _configurationProperties;

		protected NHibernate.Cfg.Configuration configuration;

		public SingletonSessionBuilder(NHibernate.Cfg.Configuration configuration)
		{
			this.configuration = configuration;
			_configurationProperties = configuration.Properties;
		}

		public ISession GetSession()
		{
			var key = this._configurationProperties["connection.connection_string"] + getHashCodeFromClassMaps ();

			// http://216.121.112.228/browse/NH-2030?page=com.atlassian.streams.streams-jira-plugin:activity-stream-issue-tab
			if (!_factories.ContainsKey(key))
				lock (syncObject)
					if (!_factories.ContainsKey(key))
						createSessionFactory(key);

			return _factories[key].OpenSession();
		}

		private int getHashCodeFromClassMaps ()
		{
			return configuration.ClassMappings.Aggregate (
				new StringBuilder (),
				( sb, next ) => sb.Append ( next.ClassName ))
					.ToString ()
					.GetHashCode ();
		}

		private void createSessionFactory(string key)
		{
			var configuration = GetConfiguration();

			_factories[key] = configuration.BuildSessionFactory();
		}

		public NHibernate.Cfg.Configuration GetConfiguration()
		{
			return configuration;
		}

		public bool ContainsMapFor<ENTITY>()
		{
			var persistentClass = this.configuration.ClassMappings.SingleOrDefault(m => m.MappedClass == typeof(ENTITY));
			if (persistentClass == null) return false;
			
			var classMapType = persistentClass.MappedClass;

			var attribute = Attribute.GetCustomAttributes(classMapType)
				.SingleOrDefault(a => a is SessionBuilderAttribute) as SessionBuilderAttribute;

			if (attribute == null) return true;
			return attribute.SessionBuilderType == this.GetType();
		}

		private Type findClassMapSubClassFor<ENTITY>(IEnumerable<Type> types)
		{
			return types.SingleOrDefault(t => t.IsSubclassOf(typeof(ClassMap<ENTITY>)));
		}
	}
}
