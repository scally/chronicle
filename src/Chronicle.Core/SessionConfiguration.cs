﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate;

namespace Chronicle
{
	public interface ISessionConfiguration
	{
		NHibernate.Cfg.Configuration GetConfiguration();
	}

	public abstract class SessionConfiguration : ISessionConfiguration
	{
		private static readonly object lockObject = new object();
		protected static Dictionary<Type, NHibernate.Cfg.Configuration> configurations;

		protected SessionConfiguration()
		{
		}

		protected abstract IPersistenceConfigurer buildConfig();
		protected abstract void modifyPersistenceModel(PersistenceModel model);

		public NHibernate.Cfg.Configuration GetConfiguration()
		{
			if (!configurationSet())
				lock(lockObject)
				{
					if ( !configurationSet ())
						return buildNewConfig ();
				}

			return getCurrentConfig();
		}

		private NHibernate.Cfg.Configuration buildNewConfig()
		{
			var config = new NHibernate.Cfg.Configuration();
			buildConfig().ConfigureProperties(config.SetProperty("config-source", this.GetType().FullName));

			config.EventListeners.PreInsertEventListeners = new NHibernate.Event.IPreInsertEventListener[] { new TimeStampEventListener() };
			config.EventListeners.PreUpdateEventListeners = new NHibernate.Event.IPreUpdateEventListener[] { new TimeStampEventListener() };

			var model = new PersistenceModel();
			modifyPersistenceModel(model);
			model.Configure(config);

			return addConfig(config);
		}

		private void ensureConfigAvailable()
		{
			if (configurations == null)
				configurations = new Dictionary<Type, NHibernate.Cfg.Configuration>();
		}

		private bool configurationSet()
		{
			ensureConfigAvailable();
			return configurations.ContainsKey(this.GetType());
		}

		private NHibernate.Cfg.Configuration addConfig(NHibernate.Cfg.Configuration config)
		{
			ensureConfigAvailable();
			configurations.Add(this.GetType(), config);
			return config;
		}

		private NHibernate.Cfg.Configuration getCurrentConfig()
		{
			ensureConfigAvailable();
			return configurations[this.GetType()];
		}
	}
}
