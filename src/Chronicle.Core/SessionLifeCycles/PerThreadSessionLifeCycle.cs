﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Threading;
using log4net;

namespace Chronicle
{
	public class PerThreadSessionLifeCycle : ISessionLifeCycle
	{
		// thread id by session
		private static Dictionary<int, Dictionary<string, ISession>> _sessions = new Dictionary<int, Dictionary<string, ISession>>();
		private static object @lock = new object();
		private static ILog logger = log4net.LogManager.GetLogger("Chronicle");

		public bool HasSession(string key)
		{
			return getCurrentThreadSessions ().ContainsKey(key);
		}

		public ISession GetSession(string key)
		{
			return getCurrentThreadSessions () [key];
		}

		public ISession AddSession(string key, ISession session)
		{
			logger.InfoFormat ( "Creating session {0} on thread {1}", session.GetHashCode (), getThreadId());
			getCurrentThreadSessions ().Add ( key, session );
			return session;
		}

		public void CloseSessions()
		{
			foreach (var session in getCurrentThreadSessions().Values)
			{
				logger.InfoFormat("Removing session {0} on thread {1}", session.GetHashCode(), getThreadId());
				session.Close();
				session.Dispose();
			}

			getCurrentThreadSessions ().Clear ();
		}

		private Dictionary<string,ISession> getCurrentThreadSessions ()
		{
			if ( ! _sessions.ContainsKey(getThreadId()))
				_sessions.Add(getThreadId(), new Dictionary<string, ISession>());

			return _sessions[getThreadId()];
		}

		private int getThreadId()
		{
			return Thread.CurrentThread.ManagedThreadId;
		}
	}
}
