﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Chronicle
{
	public class TransientSessionLifeCycle : ISessionLifeCycle
	{
		private Dictionary<string,ISession> sessions = new Dictionary<string,ISession> ();

		public bool HasSession(string key)
		{
			verifySessionsNotEmpty ();
			return sessions.ContainsKey(key);
		}

		public NHibernate.ISession GetSession(string key)
		{
			verifySessionsNotEmpty();
			return sessions[key];
		}

		public NHibernate.ISession AddSession(string key, NHibernate.ISession session)
		{
			verifySessionsNotEmpty();
			sessions.Add(key, session);
			return session;
		}

		public void CloseSessions()
		{
			verifySessionsNotEmpty();

			foreach ( var session in sessions.Values )
			{
				session.Close ();
				session.Dispose ();
			}

			sessions = null;
		}

		private void verifySessionsNotEmpty()
		{
			if (sessions == null)
				sessions = new Dictionary<string, ISession>();
		}
	}
}
