﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Event;
using NHibernate.Persister.Entity;

namespace Chronicle
{
	public class TimeStampEventListener : IPreUpdateEventListener, IPreInsertEventListener
	{
		private bool isValidEntity(object entity)
		{
			return entity != null;
		}

		public bool OnPreInsert(PreInsertEvent @event)
		{
			var timestamped = (@event.Entity as ITimestamped);
			if (!isValidEntity(timestamped)) return false;

			timestamped.Timestamp = new Timestamp();

			updateState(@event.Persister, @event.State, "Timestamp", timestamped.Timestamp);

			return false;
		}

		public bool OnPreUpdate(PreUpdateEvent @event)
		{
			var timestamped = (@event.Entity as ITimestamped);
			if (!isValidEntity(timestamped)) return false;

			if (timestamped.Timestamp == null)
				timestamped.Timestamp = new Timestamp();
			timestamped.Timestamp.Modify();

			updateState(@event.Persister, @event.State, "Timestamp", timestamped.Timestamp);

			return false;
		}

		private void updateState(
			IEntityPersister persister,
			object[] state,
			string propertyName,
			Timestamp value)
		{
			int i = Array.IndexOf(persister.PropertyNames, propertyName);
			if (i < 0) return;

			state[i] = value;
		}
	}
}
