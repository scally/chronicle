﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chronicle
{
	public interface ITimestamped
	{
		Timestamp Timestamp { get; set; }
	}

	public class Timestamp 
	{
		public virtual DateTime DateCreated { get; set; }
		public virtual DateTime DateModified { get; set; }

		public Timestamp ()
		{
			DateCreated = SystemTime.Now ();
			DateModified = SystemTime.Now();
		}

		public void Modify ()
		{
			DateModified = SystemTime.Now ();
		}

		public override bool Equals(object obj)
		{
			var ts = obj as Timestamp;
			if (ts == null) return false;

			return ts.DateCreated == DateCreated
				&& ts.DateModified == DateModified;
		}
	}
}
