﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Linq;
using System.Linq.Expressions;
using System.Collections;

namespace Chronicle
{
	// http://marcinbudny.blogspot.com/2009/10/typed-expand-for-linq-to-nhiberante.html

	public static class TypedExpandExtension
	{
		public static INHibernateQueryable<T> Expand<T, K, L>(
			this INHibernateQueryable<T> nhQueryable,
			Expression<Func<T, K>> selector, SubpathBuilder<L> subpaths)
		{
			var mainPath = ProcessSelector(selector);

			foreach (var subPathSelector in subpaths.Expressions)
			{
				var subpath = ProcessSelector(subPathSelector);
				nhQueryable.Expand(mainPath + "." + subpath);
			}

			return nhQueryable;
		}

		public static INHibernateQueryable<T> Expand<T> (
				this INHibernateQueryable<T> nhQueryable,
				LambdaExpression selector)
		{
			if (selector.Body.NodeType != ExpressionType.MemberAccess)
				throw new ArgumentException("Selector has to be of MemberAccess type", "selector");

			nhQueryable.Expand(ProcessSelector(selector));

			return nhQueryable;
		}

		public static INHibernateQueryable<T> Expand<T, K>(
			this INHibernateQueryable<T> nhQueryable,
			Expression<Func<T, K>> selector)
		{
			if (selector.Body.NodeType != ExpressionType.MemberAccess)
				throw new ArgumentException("Selector has to be of MemberAccess type", "selector");

			nhQueryable.Expand(ProcessSelector(selector));

			return nhQueryable;
		}

		private static string ProcessSelector(LambdaExpression expression)
		{
			var memberStack = new Stack<string>();
			TraverseMemberAccess(expression.Body as MemberExpression, memberStack);

			return JoinMembers(memberStack);
		}

		private static void TraverseMemberAccess(MemberExpression expression,
			Stack<string> memberStack)
		{
			if (expression != null)
			{
				memberStack.Push(expression.Member.Name);

				var methodCall = expression.Expression as MethodCallExpression;
				if (expression.Expression is MethodCallExpression)
				{
					TraverseIndexerCall(expression.Expression as MethodCallExpression,
						memberStack);
				}
				else if (expression.Expression is MemberExpression)
				{
					TraverseMemberAccess(expression.Expression as MemberExpression,
						memberStack);
				}
				else if (!(expression.Expression is ParameterExpression))
				{
					throw new ArgumentException("Selector contains invalid expression of type"
						+ expression.Expression.NodeType, "selector");
				}
			}
		}

		private static void TraverseIndexerCall(MethodCallExpression methodCall,
			Stack<string> memberStack)
		{
			if (methodCall != null)
			{
				var type = methodCall.Object.Type;

				var interfaces = type.GetInterfaces().Select(i => i.Name).ToList();
				if (type.IsInterface)
					interfaces.Add(type.Name);

				if ((interfaces.Contains("IList") || interfaces.Contains("IList`1"))
					&& (methodCall.Method.Name == "get_Item"))
				{
					TraverseMemberAccess(methodCall.Object as MemberExpression,
						memberStack);
				}
				else
				{
					throw new ArgumentException("Selector contains invalid method call",
						"selector");
				}
			}
		}

		private static string JoinMembers(Stack<string> memberStack)
		{
			return string.Join(".", memberStack.ToArray());
		}


	}

	public static class Subpath
	{
		public static SubpathBuilder<T> For<T>()
		{
			return new SubpathBuilder<T>();
		}
	}

	public class SubpathBuilder<T>
	{
		public IList<LambdaExpression> Expressions { get; private set; }

		public SubpathBuilder()
		{
			Expressions = new List<LambdaExpression>();
		}

		public SubpathBuilder<T> Add<K>(Expression<Func<T, K>> selector)
		{
			if (selector.Body.NodeType != ExpressionType.MemberAccess)
				throw new ArgumentException("Selector has to be of MemberAccess type", "selector");

			Expressions.Add(selector);
			return this;
		}
	}
}
